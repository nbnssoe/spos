import java.util.Scanner;
import java.io.*;
public class safe {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Number Of Processes:-");
		int P=sc.nextInt();
		System.out.println("Enter the Number Of Resources");
		int R=sc.nextInt();
		int alloc[][]=new int[P][R];
		int max[][]=new int[P][R];
		int need[][]=new int[P][R];
		int available[][]=new int[1][R];
		int count=0;
		String sequence="";
		System.out.println("Enter the Entries in the Allocation table");
		for(int i=0;i<P;i++)
		{
			for(int j=0;j<R;j++)
			{
				alloc[i][j]=sc.nextInt();
			}
		}
		System.out.println("Enter the Entries in the Max. table");
		for(int i=0;i<P;i++)
		{
			for(int j=0;j<R;j++)
			{
				max[i][j]=sc.nextInt();
				need[i][j]=max[i][j]-alloc[i][j];
			}
		}
		System.out.println("Enter the Entries in the Available table");
		for(int i=0;i<R;i++)
		{
			available[0][i]=sc.nextInt();
		}
		int work[][]=new int[1][R];
		work=available;
		boolean finish[]=new boolean[P];
		for(int i=0;i<P;i++)
			finish[i]=false;
		boolean status=false;
		while(!status)
		{
				for(int i=0;i<P;i++)
				{
					if(!finish[i])
					{
						int j;
						for(j=0;j<R;j++)
						{
							if(need[i][j] > work[0][j])
								break;
						}
						if(j==R)
						{
							for(j=0;j<R;j++)
							{
								work[0][j]=work[0][j]+alloc[i][j];
							}
							finish[i]=true;
							sequence=sequence+i+",";
						}
					}
				}
				for(int i=0;i<P;i++)
				{
					if(finish[i]==true)
						count++;
				}
				if(count==P)
				{
					status=true;
					break;
				}					
		}
		System.out.println("The Entries in the Need table");
		for(int i=0;i<P;i++)
		{
			System.out.print("[");
			for(int j=0;j<R;j++)
			{
				System.out.print(need[i][j]+"	");
				
			}
			System.out.println("]");
		}
		System.out.println("Work Matrix is:["+work[0][0]+"	"+work[0][1]+"	"+work[0][2]+"  ]");
		if(count==P)
		{
			System.out.println("In Safe State");
			System.out.println("The Sequence Of Excecution Is===>  "+ sequence);
		}
		else
			System.out.println("Not In Safe State");
	}

}
/*
student@student-OptiPlex-3020:~$ javac safe.java
student@student-OptiPlex-3020:~$ java safe
Enter the Number Of Processes:-
5
Enter the Number Of Resources
3
Enter the Entries in the Allocation table
0 1 0
2 0 0
3 0 2
2 1 1
0 0 2
Enter the Entries in the Max. table
7 5 3
3 2 2
9 0 2
2 2 2
4 3 3
Enter the Entries in the Available table
3 3 2
The Entries in the Need table
[7	4	3	]
[1	2	2	]
[6	0	0	]
[0	1	1	]
[4	3	1	]
Work Matrix is:[10	5	7  ]
In Safe State
The Sequence Of Excecution Is===>  1,3,4,0,2,
student@student-OptiPlex-3020:~$ 

*/
