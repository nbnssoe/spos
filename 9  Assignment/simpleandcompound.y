%{
	#include<stdio.h>
	int flag=0;
%}
%token NOUN PRONOUN VERB ADVERB ADJECTIVE PREPOSITION CONJUNCTION

%%
sentence : simple_sentence	{printf("simple sentence. \n");} | compound_sentence	{printf("compound sentence. \n");};

simple_sentence : subject verb object |subject verb object prep_phrase     {flag=0;};
compound_sentence : simple_sentence CONJUNCTION simple_sentence | compound_sentence CONJUNCTION simple_sentence  {flag=1;};
subject: NOUN|PRONOUN|ADJECTIVE subject;
verb : VERB|ADVERB VERB | verb VERB;
object : NOUN | ADJECTIVE object;
prep_phrase: PREPOSITION NOUN;
 

%%


int yyerror(char *str)
{
printf("error");
return 1;
}

int yywrap()
{return 1;}

main()
{
printf("enter: ");
yyparse();
}
