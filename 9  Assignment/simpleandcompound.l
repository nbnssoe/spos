%{
#include<stdio.h>
#include "y.tab.h"
%}

%%
[\t ]+ {/*Ignore white spaces*/};
is|am|are							{return VERB;}
very|simple|gently					{return ADVERB;}
to|from|behind 					{return PREPOSITION;}
if|then|and 						{return CONJUNCTION;}
their|my|your						{return ADJECTIVE;}
i|you|he|she						{return PRONOUN;}
[a-zA-Z]+							{return NOUN;}

%%

