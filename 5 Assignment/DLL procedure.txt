Implement a program for Dynamic link library for any mathematical operation use java native interface


A dynamic link library (DLL) is a collection of small programs that can be loaded when needed by larger programs and used at the same time. 

java path=/usr/lib/jvm/java-7-openjdk-amd64/  for our college Systems

student@student-OptiPlex-3020:~$ gedit TestJNI.java
public class TestJNI 
{ 
     static { //a static block is executed only once, when your program begins execution
                System.loadLibrary("cal");  // Load native library at runtime 
			        // cal.dll (Windows) or libcal.so (Unix)
            } 
	// Declare a native method add() that receives nothing and returns void
	 private native int add(int n1,int n2);
 	// Test Driver 
	public static void main(String[] args) 
	{ 
	// invoke the native method
  		 System.out.println("Addition is="+new TestJNI().add(10,20)); 
	}
 }
 
After completion of  program, compile it to produce TestJNI.class file. Next, you must use javah.exe to produce one file: TestJNI.h. You will include TestJNI.h in your implementation of add( ). To produce TestJNI.h, use the following command:
javah -jni TestJNI

This command produces a header file called TestJNI.h. This file must be included in the C file that implements add( ). 
 
student@student-OptiPlex-3020:~$ gedit TestJNI.c
#include<jni.h>  // which contains interfacing information. This file is provided by your Java compiler
#include<stdio.h>
#include "TestJNI.h" //The header file TestJNI.h was created by javah, earlier.
JNIEXPORT jint JNICALL Java_TestJNI_add(JNIEnv *env,jobject thisObj,jint n1,jint n2)
{ 
  jint res;
   res=n1+n2;
   return res;
}   



student@student-OptiPlex-3020:~$ javac TestJNI.java
student@student-OptiPlex-3020:~$ javah -jni TestJNI
student@student-OptiPlex-3020:~$ gcc -I /usr/lib/jvm/java-7-openjdk-amd64/include/ -I /usr/lib/jvm/java-7-openjdk-amd64/include/linux/ -o libcal.so -shared TestJNI.c

student@student-OptiPlex-3020:~$ java -Djava.library.path=. TestJNI 
// Configure your java runtime environment to include the directory of the shared library
Addition is=30
student@student-OptiPlex-3020:~$ 



Java Native Interface (JNI)The standard is part of the Java platform, 
which allows Java code and other language to write code to interact.
-I option in gcc is used for directory search
-shared option for linking

JNIEXPORT jint JNICALL Java_TestJNI_add(JNIEnv *env,jobject thisObj,jint n1,jint n2)
When the JVM invokes the function, it passes a JNIEnv pointer, a jobject pointer, 
and any Java arguments declared by the Java method. 


